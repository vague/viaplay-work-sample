# Readme

Exposes an API that takes a Viaplay movie resource link and returns a Trailer Addict movie trailer URL.

## Installation

    $ npm install

## Testing

    $ npm run eslint
    $ npm test

I'm not quite sure about the distinction between *system* and *unit* tests, but my take on it is that *system* tests should focus on the purpose of the application, and *unit* tests should focus on individual components.

By this definition the *test/trailer-api-service.js* would be system tests, and *test/callback-queue.js* would be unit tests.

## Running

    $ npm start

You may query the API with curl like this:

    curl -v --get http://localhost:8080/api/trailer -H "Accept: application/json" --data-urlencode "movieUrl=https://content.viaplay.se/web-se/film/the-internship-2013"


## Scalability

The specification states:

> Assume that your app will have tens of thousands of requests per second at peak times.

I did not have enough time to complete this task, but I would approach it by:

1.  Implementing [Cluster](https://nodejs.org/api/cluster.html)
2.  Implementing a cache mechanism for `TrailerApiService#getTrailerUrl`
    *   Probably with a memcached, mongo or redis server

## Todo

*   Scalability
*   The API should respond with HAL resources with links
*   Parse errors from the Viaplay and Trailer Addict APIs
    *   Currently they're returned as `Response`s by the `request-promise` module
*   Many more unit tests!
