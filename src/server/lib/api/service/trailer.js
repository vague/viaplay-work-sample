'use strict';

var _ = require('lodash'),
    Promise = require('bluebird'),
    express = require('express'),
    ApiProblem = require('../problem');

function TrailerApiService(viaplayApi, trailerAddictApi) {
    this.viaplayApi = viaplayApi;
    this.trailerAddictApi = trailerAddictApi;
    this.router = express.Router();
    this.router.get('/', this.getTrailerUrl.bind(this));
}

TrailerApiService.factory = function (services) {
    return new TrailerApiService(
        services.get('ViaplayApi'),
        services.get('TrailerAddictApi')
    );
};

TrailerApiService.prototype.getTrailerUrl = function (req, res, next) {
    var movieUrl = req.query.movieUrl,
        moviePublicPath;

    if (!movieUrl) {
        return next(new ApiProblem(422, 'Validation failed', null, null, {
            messages: {
                movieUrl: 'Value is required'
            }
        }));
    }

    moviePublicPath = movieUrl.split('/').pop();
    if (!moviePublicPath) {
        return next(new ApiProblem(422, 'Validation failed', null, null, {
            messages: {
                movieUrl: 'Unable to parse value'
            }
        }));
    }

    Promise.resolve(moviePublicPath).bind(this).then(function (path) {
        return this.viaplayApi.getMovieEntity(path);
    }).then(function (entity) {
        var imdbId = _.get(entity, '_embedded.viaplay:blocks[0]._embedded.viaplay:product.content.imdb.id');
        if (!imdbId) {
            return Promise.reject(new ApiProblem(404, 'Unable to fetch IMDB identifier'));
        }
        return imdbId;
    }).then(function (id) {
        return this.trailerAddictApi.getTrailerCollectionByImdbId(id);
    }).then(function (collection) {
        var trailerUrl = _.get(collection, 'trailers.trailer[0].link[0]');
        if (!trailerUrl) {
            return Promise.reject(new ApiProblem(404, 'Unable to fetch trailer URL'));
        }
        return {
            trailerUrl: trailerUrl
        };
    }).then(function (result) {
        res.json(result);
    }).catch(function (error) {
        next(error);
    });
};

module.exports = TrailerApiService;
