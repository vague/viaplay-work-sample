'use strict';

var request = require('request-promise');

function ViaplayApi() {
    this.endpoint = 'https://content.viaplay.se/web-se';
}

ViaplayApi.factory = function () {
    return new ViaplayApi();
};

ViaplayApi.prototype.getMovieEntity = function (publicPath) {
    return request({
        method: 'GET',
        baseUrl: this.endpoint,
        uri: '/film/' + publicPath,
        headers: {
            accept: 'application/json'
        }
    }).then(JSON.parse);
};

module.exports = ViaplayApi;
