'use strict';

var _ = require('lodash'),
    util = require('util');

function ServiceManager() {
    this.aliases = {};
    this.factories = {};
    this.pendingFactories = {};
    this.alwaysCreate = false;
    this.instances = {};
}

ServiceManager.prototype.resolveAlias = function (alias) {
    var stack = [];
    while (this.aliases[alias]) {
        if (stack.indexOf(alias) !== -1) {
            throw new Error(util.format(
                'Circular reference: %s -> %s.',
                stack.join(' -> '),
                alias
            ));
        }
        stack.push(alias);
        alias = this.aliases[alias];
    }
    return alias;
};

ServiceManager.prototype.alias = function (alias, name) {
    this.aliases[alias] = name;
    return this;
};

ServiceManager.prototype.create = function (name, options) {
    var instance, error;
    name = this.resolveAlias(name);
    if (!this.factories[name]) {
        throw new Error(util.format(
            'Service not found: %s.',
            name
        ));
    }
    if (this.pendingFactories[name]) {
        throw new Error(util.format(
            'Circular dependency: %s.',
            name
        ));
    }
    try {
        this.pendingFactories[name] = true;
        instance = this.factories[name](this, options, name);
        delete this.pendingFactories[name];
    } catch (factoryError) {
        error = new Error(util.format(
            'Service not created: %s. Previous error: %s',
            name,
            factoryError.message
        ));
        error.previous = factoryError;
        throw error;
    }
    if (!instance) {
        throw new Error(util.format(
            'Service not created: %s. Factory was called but did not return an instance.',
            name
        ));
    }
    if (this.validateService) {
        try {
            this.validateService(instance);
        } catch (error) {
            throw new Error(util.format(
                'Invalid service: %s. Previous error: %s',
                name,
                error
            ));
        }
    }
    return instance;
};

ServiceManager.prototype.register = function (name, factory) {
    if (!_.isFunction(factory)) {
        throw new Error('Factory must be a function.');
    }
    name = this.resolveAlias(name);
    this.factories[name] = factory;
    return this;
};

ServiceManager.prototype.get = function (name) {
    if (this.alwaysCreate) {
        return this.create(name);
    }
    name = this.resolveAlias(name);
    if (!this.instances[name]) {
        this.inject(name, this.create(name));
    }
    return this.instances[name];
};

ServiceManager.prototype.inject = function (name, instance) {
    if (this.validateService) {
        try {
            this.validateService(instance);
        } catch (error) {
            throw new Error(util.format(
                'Invalid service: %s. Previous error: %s',
                name,
                error
            ));
        }
    }
    name = this.resolveAlias(name);
    this.instances[name] = instance;
    return this;
};

module.exports = ServiceManager;
