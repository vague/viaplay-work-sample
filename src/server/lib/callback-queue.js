'use strict';

var _ = require('lodash'),
    Promise = require('bluebird'),
    PriorityQueue = require('priorityqueuejs');

function CallbackQueue() {
    this.queue = new PriorityQueue(function (a, b) {
        if (a.priority > b.priority) {
            return 1;
        }
        if (a.priority < b.priority) {
            return -1;
        }
        return 0;
    });
}

CallbackQueue.defaultPriority = 1;

CallbackQueue.defaultIterator = function (callback) {
    return callback();
};

CallbackQueue.prototype.insert = function (callback, priority) {
    if (!_.isFunction(callback)) {
        throw new Error('Invalid argument: callback must be a function');
    }
    if (priority === undefined) {
        priority = CallbackQueue.defaultPriority;
    }
    this.queue.enq({
        callback: callback,
        priority: priority
    });
    return this;
};

CallbackQueue.prototype.run = function (iterator) {
    /* Sadly PriorityQueue does not implement the array interface, so we have
       to convert it to an array for Promise.each like this - otherwise we'd
       just be re-inventing Promise.each anyway. */
    var items = [];
    iterator = Promise.method(iterator || CallbackQueue.defaultIterator);
    while (!this.queue.isEmpty()) {
        items.push(this.queue.deq());
    }
    return Promise.each(items, function (item) {
        return iterator(item.callback, item.priority);
    }).return(this);
};

module.exports = CallbackQueue;
