'use strict';

var Promise = require('bluebird'),
    request = require('request-promise'),
    parseXmlString = Promise.promisify(require('xml2js').parseString);

/**
 * @see http://www.traileraddict.com/trailerapi
 */
function TrailerAddictApi() {
    this.endpoint = 'http://api.traileraddict.com';
}

TrailerAddictApi.factory = function () {
    return new TrailerAddictApi();
};

TrailerAddictApi.prototype.getTrailerCollectionByImdbId = function (imdbId) {
    /* Documentation: If you search IMDB, you will see the ID number tt1403865
       in the URL. That is the ID, but we only want the numbers, not the
       opening "tt". */
    if (('' + imdbId).substr(0, 2) === 'tt') {
        imdbId = imdbId.substr(2);
    }
    return request({
        method: 'GET',
        baseUrl: this.endpoint,
        uri: '/',
        qs: {
            imdb: imdbId
        },
        headers: {
            accept: 'application/xml'
        }
    }).then(parseXmlString);
};

module.exports = TrailerAddictApi;
