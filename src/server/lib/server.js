'use strict';

var Promise = require('bluebird'),
    http = require('http'),
    winston = require('winston'),
    ServiceManager = require('./service-manager'),
    CallbackQueue = require('./callback-queue');

function Server(port) {
    this.port = port || 3000;
    this.services = new ServiceManager();
    this.initializers = new CallbackQueue();

    this.services.register('HttpServer', function (services) {
        var httpServer = http.createServer(services.get('RequestListener'));
        httpServer.on('error', function (error) {
            winston.error('Server error:', error);
            process.exit(-1);
        });
        httpServer.on('listening', function () {
            var addr = httpServer.address();
            winston.info('Listening on %s:%d.', addr.address, addr.port);
        });
        return httpServer;
    });
}

Server.prototype.start = function () {
    var server = this;
    return this.initializers.run(function (callback) {
        return callback(server.services);
    }).then(function () {
        var httpServer = server.services.get('HttpServer');
        return new Promise(function (resolve) {
            httpServer.on('listening', function () {
                resolve(server);
            });
            httpServer.listen(server.port);
        });
    });
};

module.exports = Server;
