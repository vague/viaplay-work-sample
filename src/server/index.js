'use strict';

var express = require('express'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    morgan = require('morgan'),
    compression = require('compression'),
    Server = require('./lib/server'),
    middleware = require('./lib/middleware'),
    api = require('./lib/api'),
    viaplay = require('./lib/viaplay'),
    trailerAddict = require('./lib/trailer-addict'),
    server;

server = new Server(8080);

server.services.register('Express', function () {
    return express();
});

server.services.alias('RequestListener', 'Express');

server.initializers.insert(function (services) {
    var app = services.get('Express');
    app.use([
        bodyParser.urlencoded({extended: false}),
        bodyParser.json(),
        methodOverride(),
        morgan('dev'),
        compression()
    ]);
}, 100);

server.initializers.insert(function (services) {
    var app = services.get('Express');
    app.use(middleware.errorHandler());
}, -100);

server.initializers.insert(api.init, 1)
                   .insert(viaplay.init, 2)
                   .insert(trailerAddict.init, 2);

module.exports = server;
