/*global describe,it,beforeEach,setTimeout*/
'use strict';

var assert = require('assert'),
    Promise = require('bluebird'),
    CallbackQueue = require('../src/server/lib/callback-queue');

describe('CallbackQueue', function () {
    var callbackQueue;

    beforeEach(function () {
        callbackQueue = new CallbackQueue();
    });

    describe('#insert', function () {
        it('fails when callback is not a function', function () {
            assert.throws(function () {
                callbackQueue.insert();
            }, Error);
        });

        it('enqueues an item in the internal priority queue', function () {
            callbackQueue.insert(function () {});
            assert.equal(1, callbackQueue.queue.size());
        });
    });

    describe('#run', function () {
        it('it returns a bluebird Promise when empty', function () {
            assert.ok(callbackQueue.run() instanceof Promise);
        });

        it('returns a bluebird Promise when not empty', function () {
            callbackQueue.insert(function () {});
            assert.ok(callbackQueue.run() instanceof Promise);
        });

        it('resolves to self', function () {
            return callbackQueue.run().then(function (actual) {
                assert(actual, callbackQueue);
            });
        });

        it('empties the internal priority queue', function () {
            callbackQueue.insert(function () {});
            return callbackQueue.run().then(function () {
                assert.ok(callbackQueue.queue.isEmpty());
            });
        });

        it('it iterates the items from high to low priority and runs them serially', function () {
            var trace = [];

            /* These return a delay for setTimeout, in order to test wether
               they are run serially */
            callbackQueue.insert(function () {
                return 0;
            }, -1);

            callbackQueue.insert(function () {
                return 0;
            }, 1);

            // If they are not run serially then 0 would appear last in trace
            callbackQueue.insert(function () {
                return 500;
            }, 0);

            return callbackQueue.run(function (callback, priority) {
                return new Promise(function (resolve) {
                    setTimeout(function () {
                        trace.push(priority);
                        resolve();
                    }, callback());
                });
            }).then(function () {
                assert.deepEqual(trace, [1, 0, -1]);
            });
        });
    });
});
