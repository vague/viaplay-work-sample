/*global describe,it*/
'use strict';

var assert = require('assert'),
    Promise = require('bluebird'),
    TrailerApiService = require('../src/server/lib/api').service.Trailer,
    ViaplayApi = require('../src/server/lib/viaplay').Api,
    TrailerAddictApi = require('../src/server/lib/trailer-addict').Api;

describe('TrailerApiService', function () {
    var api = new TrailerApiService(
        new ViaplayApi(),
        new TrailerAddictApi()
    );

    describe('#getTrailerUrl', function () {
        var getTrailerUrl = function (req) {
            return new Promise(function (resolve, reject) {
                api.getTrailerUrl(req, {
                    json: resolve
                }, reject);
            });
        };

        this.timeout(25000);

        it('should return a 422 error if movieUrl is omitted', function () {
            return getTrailerUrl({
                query: {}
            }).then(
                function () {
                    return Promise.reject(new Error('Expected an error'));
                },
                function (error) {
                    assert.equal(error.status, 422);
                }
            );
        });

        it('should return the expected trailer url', function () {
            var inputUrl = 'https://content.viaplay.se/web-se/film/the-internship-2013',
                outputUrl = 'http://www.traileraddict.com/the-internship/tv-spot-hbo';
            return getTrailerUrl({
                query: {
                    movieUrl: inputUrl
                }
            }).then(function (result) {
                assert.equal(result.trailerUrl, outputUrl);
            });
        });
    });
});
